# Jack'n the Box Quiz Shop
Android quiz game app I original created in 2017.

### 🗒️Overview

```
- 3 categories: Disney, Harry Potter, Spongebob Squarepants.
- Each category consists of a mix of 10 multiple choice/text input questions.
- Displays quiz results with pop-up message based on result.
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

|                        Home Page                        |
|:-------------------------------------------------------:|
| ![Home page](public/screens/homepage.webp){width=300px} |

|                            Disney Questions                            |                          Harry Potter Questions                          |                    Spongebob Squarepants Questions                    |
|:----------------------------------------------------------------------:|:------------------------------------------------------------------------:|:---------------------------------------------------------------------:|
| ![Disney questions](public/screens/questions-disney.webp){width=300px} | ![Harry Potter questions](public/screens/questions-hp.webp){width=300px} | ![Spongebob questions](public/screens/questions-sb.webp){width=300px} |

|                            Disney Submit Button                             |                          Harry Potter Submit Button                           |                    Spongebob Squarepants Submit Button                     |
|:---------------------------------------------------------------------------:|:-----------------------------------------------------------------------------:|:--------------------------------------------------------------------------:|
| ![Disney submit button](public/screens/submit-btn-disney.webp){width=300px} | ![Harry Potter submit button](public/screens/submit-btn-hp.webp){width=300px} | ![Spongebob submit button](public/screens/submit-btn-sb.webp){width=300px} |

|                           Disney Results                           |                         Harry Potter Results                         |                   Spongebob Squarepants Results                   |
|:------------------------------------------------------------------:|:--------------------------------------------------------------------:|:-----------------------------------------------------------------:|
| ![Disney results](public/screens/results-disney.webp){width=300px} | ![Harry Potter results](public/screens/results-hp.webp){width=300px} | ![Spongebob results](public/screens/results-sb.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
